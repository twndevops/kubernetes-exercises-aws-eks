#### Notes for "Module 11 - Kubernetes on AWS EKS" exercises

- MODULE 10 GOAL: configure and deploy Java-Gradle app manually to LKE cluster.
- MODULE 11 GOAL: configure and automate deploying Java-Gradle app to EKS cluster.
- Can find Java app on `main` branch and solutions on `feature` branch, here: https://gitlab.com/twn-devops-bootcamp/latest/11-eks/eks-exercises
  - Used [this repo's](https://gitlab.com/twndevops/kubernetes-exercises) Dockerfile to build app.

EXERCISE 1: Create EKS (AWS-managed K8s service) cluster w/ 3 Nodes and 1 Fargate profile. Use `eksctl` to simplify cluster creation/ configuration.

- Ran `eksctl create cluster -f ekscluster-fp-ng.yaml`. (20 mins to complete)
  - Checks:
    - `java-app-cluster` created w/:
      - a Nodegroup w/ 3 m5.large nodes
      - a Fargate profile w/ provided namespace and label selector
    - VPC `eksctl-java-app-cluster-cluster` created
    - 3 IAM roles created (EKS ServiceRole, NodeInstanceRole, FargatePodExecutionRole)
    - 2 CloudFormation stacks, for Cluster and Nodegroup
- Cleanup cmd: `eksctl delete cluster java-app-cluster`
  - If this throws a LOT of `1 pods are unevictable from node ip-192-168-24-160.ec2.internal` errors, manually delete resources from AWS Management Console.
  - WAIT 5 mins. Resource deletions cascade after that.
  - Total time to delete: 12 mins.

EXERCISE 2: Deploy Mysql and phpmyadmin on the EC2 nodes using the same setup as before (Module 10).

- Fargate does NOT support stateful apps (DBs like MySQL) or DaemonSets, so deploying MySQL on Nodegroup EC2's and the stateless Java app on Fargate.

- Fargate will only schedule pods deployed in default NS w/ label `profile: fargate`. The following lack the label, so they will run on the EC2s.

- `cd k8s-components`. In order:

  - MySQL StatefulSets via Helm chart install passing overrides file: `helm install mysql -f mysqldb.yaml bitnami/mysql`
    - Creates `mysql` secret w/ passwords.
    - ISSUE when using t2.micro EC2s: `pod/mysql-secondary-1` never reached READY state, likely due to too few resources on node.
      - LEARNING: Start cluster using node instanceType w/ sufficient resources to run all apps in cluster, including autoscaler!
  - `proj-secret` secret: `kubectl apply -f secret.yaml`
  - `proj-configmap` cm: `kubectl apply -f configmap.yaml`
  - phpmyadmin deploy (depends on `proj-secret` and `proj-configmap`) and svc: `kubectl apply -f phpmyadmin.yaml`
    - Port forward to access phpmyadmin UI on localhost: `kubectl port-forward service/phpmyadmin-svc :80`.
      - Confirmed root and nonroot user creds work to login / access `mysqldb` DB.

- Check all pods above deployed on Nodegroup EC2s.
  - All 3 nodes or fewer may be distributed pod(s).

EXERCISE 3: Deploy your Java app using Fargate with 3 replicas

- This deployment used image from public DockerHub repo.
  - FOLLOWUP FOR LATER TIME: Set up deployment to pull image from private DockerHub repo.
- Added label `profile: fargate` to deployment spec selector matchLabels and pod labels so app pods are scheduled by Fargate.
- `kubectl apply -f java-gradle-app.yaml`
- `kubectl get pods -o wide` > NODE column indicates 3 app replicas were scheduled on Fargate nodes b/c node IPs are prefixed w/ `fargate-ip-`.
  - Fargate provisions a VM (node) per pod. Each Fargate node IP is different.
- `kubectl describe pods` to check Fargate pods pulled correct image.
- `kubectl delete deploy [deployment name]` deletes the pods AND Fargate nodes!

EXERCISE 4: Set up CD (automate deployment) to cluster in Jenkins pipeline

- Skipped `increment version` and `commit version bump` stages.

- In Jenkins UI, created `java-gradle-build` multibranch pipeline (though there is only 1 branch, `main`).
  - In Configure > Branch Sources, set Project Repo to `https://gitlab.com/twndevops/kubernetes-exercises-aws-eks` and Discover branches using `Filter by name (with wildcards)` > `Include *`.

EXERCISE 5: Use ECR to host app Docker images (to have everything on AWS platform), and update pipeline to use ECR repo.

- In AWS ECR, created new private repo called `java-gradle-app-eks`.

  - Once created, click `View push commands`.
    - `aws ecr get-login-password` to get ECR password for configured AWS user.
      - Can check configured user in `.aws/credentials` or by running `aws configure list`.
    - `docker login --username AWS` gives you the username.

- For `build image` stage:

  - Configured `Username with password` ECR creds in Jenkins, scoped globally since these creds can be used to authenticate Jenkins to any ECR repo.
  - Created env vars for ECR registry URL and repo name in `environment` block outside stages, scoping them globally for use in any stage and in `java-gradle-app.yaml`.

- For `deploy to EKS` stage:

- Added `.kube/config` file to Jenkins container so Jenkins agent can execute `kubectl` cmds against `java-app-cluster` using `aws-iam-authenticator` tool.

  - Created `config` on remote (Digital Ocean) server running Jenkins container.
  - Copied file to Jenkins container: `docker cp config [container ID]:/var/jenkins_home/.kube`
    - Cannot create config file on Jenkins container directly since Docker container lacks editor (vim).
    - MUST name file `config` for `kubectl` to find cluster's API server (public) endpt.
    - `/var/jenkins_home` = working dir when `docker exec` into container as Jenkins user
    - `/root` = working dir when `docker exec` into container as root user (`-u 0`)

- Configured 2 `Secret text` creds for user access/secret key pair in Jenkins, scoped globally since these creds can be used to auth Jenkins to AWS as user `twnadmin`.

  - Stored creds in `environment` block vars, scoped to `deploy` stage, for Jenkins agent to reference to auth to AWS.

- Replaced global env vars for ECR registry URL and repo name in `java-gradle-app.yaml` deployment and svc templates.

  - `envsubst` tool will replace vars w/ Jenkinsfile values to create tmp config files to deploy both components.

- Pushed changes to Git and manually built `java-gradle-build` pipeline in Jenkins UI.

PROGRESS SO FAR: App changes are now automatically deployed in the cluster.

EXERCISE 6: Configure autoscaling to scale down to a minimum of 1 node when servers are underutilized and maximum of 3 nodes when in full use.

- Created custom policy `ng-autoscale-policy-java-app-cluster` to give Nodegroup EC2's permissions to make API calls to autoscale EC2s.

  - Attached policy to Nodegroup's `NodeInstanceRole` role.
  - Policies are independent of roles. Though a role is deleted, its attached policies aren't.

- Double-check 2 tags `k8s.io/cluster-autoscaler/java-app-cluster` and `k8s.io/cluster-autoscaler/enabled` were created on Autoscaling Group, so K8s autoscaler component will be able to discover AWS AG.

- [K8s autoscaler config](https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml)

  - Modified deployment config:
    - added annotation `cluster-autoscaler.kubernetes.io/safe-to-evict:false` to metadata
    - updated image version to `v1.28.2`
    - `--node-group-auto-discovery` w/ cluster name
      - this param passes the 2 tags for the K8s autoscaler to discover the AWS AG
    - added `--balance-similar-node-groups` and `--skip-nodes-with-system-pods=false` params to cmd
  - Deploy K8s autoscaler component stack to `kube-system` NS: `kubectl apply -f cluster-autoscaler-autodiscover.yaml`.
    - ERROR when using t2.micro EC2s and running `kubectl describe pod [autoscaler pod ID] -n kube-system`:
      - `0/6 nodes are available: 2 Too many pods, 3 Insufficient memory, 3 node(s) had untolerated taint {eks.amazonaws.com/compute-type: fargate}. preemption: 0/6 nodes are available: 1 No preemption victims found for incoming pod, 2 Insufficient memory, 3 Preemption is not helpful for scheduling.`
      - TRIED: Scaling to 4 t2.micro EC2s. Got similar error as above w/ `0/7 nodes are available`.
      - FIX: Restarted cluster w/ default m5.large EC2s.
      - LEARNING: Start cluster using node instanceType w/ sufficient resources to run all apps in cluster, including autoscaler!
  - `kubectl describe pod [autoscaler pod ID] -n kube-system` > check Events to ensure
    deployment image (`registry.k8s.io/autoscaling/cluster-autoscaler:v1.28.2`) was pulled and container is up.
    - Pod runs on a Nodegroup EC2.
  - `kubectl logs cluster-autoscaler-86ddc88759-vbb5j -n kube-system`

    - Keep an eye out for:

      - `Calculating unneeded nodes`
      - `Starting scale down`
      - `ip-192-168-33-209.ec2.internal was unneeded for 20.170299783s`
      - `node ip-192-168-33-209.ec2.internal may be removed`
      - `Scale-down calculation: ignoring 2 nodes unremovable in the last 5m0s`
      - `ip-192-168-33-209.ec2.internal for removal`
      - `1 event_sink_logging_wrapper.go:48] Event(v1.ObjectReference{Kind:"Node", Namespace:"", Name:"ip-192-168-56-99.ec2.internal", UID:"5b62a309-59a8-4a93-b108-1595b09aad16", APIVersion:"v1", ResourceVersion:"16588", FieldPath:""}): type: 'Normal' reason: 'ScaleDown' marked the node as toBeDeleted/unschedulable`

      - NOTE: `Node fargate-ip-192-168-83-152.ec2.internal should not be processed by cluster autoscaler (no node group config)`
        - You've set up the cluster autoscaler to auto-discover the AWS AG using tags. However, the pod also discovers/ignores the Fargate nodes.

- Took some time for the autoscaler to actually scale down the Nodegroup. Ran `eksctl scale nodegroup --cluster=java-app-cluster --nodes=3 --name=java-app-nodes --nodes-min=2 --wait` to increase min from 1 to 2, kept desired 3. The autoscaler then decreased the nodes from 3 to 2.
  - In logs, `Successfully added DeletionCandidateTaint on node ip-192-168-44-29.ec2.internal`. A taint prevents any new workload from being scheduled on that node.

FOLLOWUPS for a later time:

1. Do Module 10 Exercises 5-6 for EKS cluster

- Deploy Ingress Controller in the cluster using Helm, so users can access Java app using domain name (not IP)
- Create Ingress rule for Java app access

2. Video 3 of Module 11 covers manually configuring autoscaling a managed Nodegroup's EC2s, not Fargate VMs. However, we're deploying the 3 Java-Gradle app replicas on Fargate nodes and all other app pods (MySQL, phpmyadmin) on the Nodegroup EC2s. Per Support, Fargate provides [a more managed approach](https://docs.aws.amazon.com/eks/latest/userguide/fargate.html) for both EKS and ECS.

- `You can use the [Vertical Pod Autoscaler](https://docs.aws.amazon.com/eks/latest/userguide/vertical-pod-autoscaler.html) to set the initial correct size of CPU and memory for your Fargate Pods, and then use the [Horizontal Pod Autoscaler](https://docs.aws.amazon.com/eks/latest/userguide/horizontal-pod-autoscaler.html) to scale those Pods. If you want the Vertical Pod Autoscaler to automatically re-deploy Pods to Fargate with larger CPU and memory combinations, set the mode for the Vertical Pod Autoscaler to either Auto or Recreate to ensure correct functionality. For more information, see the [Vertical Pod Autoscaler documentation on GitHub](https://github.com/kubernetes/autoscaler/tree/master/vertical-pod-autoscaler#quick-start).`
