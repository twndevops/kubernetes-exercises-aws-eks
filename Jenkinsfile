#!/usr/bin/env groovy

pipeline {
    // any Jenkins agent can execute this pipeline
    agent any
    // declarative step to install Gradle
    tools {
        gradle 'gradle-8.6-milestone-1'
    }
    // global env vars that can be used across pipeline stages and in app deploy file
    environment {
        ECR_URL="297794876573.dkr.ecr.us-east-1.amazonaws.com"
        APP_NAME="java-gradle-app-eks"
    }
    stages {
        stage('build app') {
            steps {
                script {
                    echo 'building the application...'
                    // clear existing build dir, build latest app artifact
                    sh 'gradle clean build'
                }
            }
        }
        stage('build image') {
            steps {
                script {
                    echo "building the docker image..."
                    // build img from app artifact
                    sh "docker build -t ${ECR_URL}/${APP_NAME}:1.0-SNAPSHOT-${BUILD_NUMBER} ."
                    // extract ECR creds into vars to docker login
                    withCredentials([usernamePassword(credentialsId: 'aws-ecr-creds-global', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
                        sh "echo $PWD | docker login --username $USER --password-stdin ${ECR_URL}"
                    }
                    // push img to private repo
                    sh "docker push ${ECR_URL}/${APP_NAME}:1.0-SNAPSHOT-${BUILD_NUMBER}"
                }
            }
        }
        stage('deploy to EKS') {
            // exporting (setting) env vars providing currently configured AWS user's (twnadmin) creds to auth/connect Jenkins to AWS
            // both keys are Secret text creds scoped globally in Jenkins
            environment {
                // can get these env var values from .aws/credentials
                AWS_ACCESS_KEY_ID = credentials('aws_access_key_global')
                AWS_SECRET_ACCESS_KEY = credentials('aws_secret_key_global')
            }
            steps {
                script {
                    echo "deploying docker image..."
                    // gettext-base pkg (w/ envsubst) installed in container
                    // env vars being substituted are global (ECR_URL and APP_NAME)
                    // kubectl runs against cluster in kubeconfig in Jenkins container
                    sh "envsubst < k8s-components/java-gradle-app.yaml | kubectl apply -f -"
                }
            }
        }
    }
}