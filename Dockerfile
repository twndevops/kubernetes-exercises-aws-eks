FROM amazoncorretto:17.0.9-al2023-headful

RUN mkdir -p app
COPY build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar /app
WORKDIR /app

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar"]